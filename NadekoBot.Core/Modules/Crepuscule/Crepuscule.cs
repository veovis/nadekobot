﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using NadekoBot.Common;
using NadekoBot.Common.Attributes;
using NadekoBot.Common.Collections;
using NadekoBot.Core.Services;
using NadekoBot.Core.Services.Database.Models;
using NadekoBot.Core.Services.Impl;
using NadekoBot.Extensions;
using NadekoBot.Modules.Music.Common;
using NadekoBot.Modules.Music.Common.Exceptions;
using NadekoBot.Modules.Music.Extensions;
using NadekoBot.Modules.Crepuscule.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace NadekoBot.Modules.Crepuscule
{
	[NoPublicBot]
	public class CrepusculeTools : NadekoTopLevelModule<CrepusculeService>
	{
		private readonly DiscordSocketClient _client;
		private readonly IBotCredentials _creds;
		private readonly DbService _db;

		public CrepusculeTools(DiscordSocketClient client,
			IBotCredentials creds,
			DbService db)
		{
			_client = client;
			_creds = creds;
			_db = db;
		}

		[NadekoCommand, Usage, Description, Aliases]
		[RequireContext(ContextType.Guild)]
		[UserPerm(GuildPerm.MoveMembers)]
		[BotPerm(GuildPerm.MoveMembers)]
		public async Task Mdc()
		{
			// cmd reserved to La confrérie du Crépuscule
			if (ctx.Guild.Id != 666719902308237353)
				return;

			var mdcRole = ctx.Guild.Roles.FirstOrDefault(r => r.Id == 679614962502467665);
			var mdcChan = await ctx.Guild.GetVoiceChannelAsync(685043240713453569);

			await MoveGroupToVoiceChannel(mdcRole, mdcChan);
		}

		[NadekoCommand, Usage, Description, Aliases]
		[RequireContext(ContextType.Guild)]
		[UserPerm(GuildPerm.MoveMembers)]
		[BotPerm(GuildPerm.MoveMembers)]
		public async Task Off()
		{
			// cmd reserved to La confrérie du Crépuscule
			if (ctx.Guild.Id != 666719902308237353)
				return;

			var offRole = ctx.Guild.Roles.FirstOrDefault(r => r.Id == 679609504907657231);
			var offChan = await ctx.Guild.GetVoiceChannelAsync(679011178105798681);

			await MoveGroupToVoiceChannel(offRole, offChan);
		}

		[NadekoCommand, Usage, Description, Aliases]
		[RequireContext(ContextType.Guild)]
		[UserPerm(GuildPerm.MoveMembers)]
		[BotPerm(GuildPerm.MoveMembers)]
		public async Task OffMdc()
		{
			// cmd reserved to La confrérie du Crépuscule
			if (ctx.Guild.Id != 666719902308237353)
				return;

			var authorCurrentVoiceChannel = ((IGuildUser)ctx.User).VoiceChannel;
			var offRole = ctx.Guild.Roles.FirstOrDefault(r => r.Id == 679609504907657231);
			var mdcRole = ctx.Guild.Roles.FirstOrDefault(r => r.Id == 679614962502467665);
			var mdcChan = await ctx.Guild.GetVoiceChannelAsync(685043240713453569);

			await InternalMoveGroupToVoiceChannel(offRole, authorCurrentVoiceChannel, mdcChan, deleteMessage: false);
			await InternalMoveGroupToVoiceChannel(mdcRole, authorCurrentVoiceChannel, mdcChan);
		}

		[NadekoCommand, Usage, Description, Aliases]
		[RequireContext(ContextType.Guild)]
		[UserPerm(GuildPerm.MoveMembers)]
		[BotPerm(GuildPerm.MoveMembers)]
		public async Task MoveGroupToVoiceChannel(IRole role, [Leftover] IVoiceChannel destVoiceChan)
		{
			var author = ctx.Message.Author;
			var authorCurrentVoiceChannel = ((IGuildUser)ctx.User).VoiceChannel;

			// author is not in voice channel
			if (authorCurrentVoiceChannel == null)
			{
				await ctx.Channel.SendMessageAsync($"Vous devez être dans un chan vocal");
				return;
			}

			await InternalMoveGroupToVoiceChannel(role, authorCurrentVoiceChannel, destVoiceChan);
		}

		async Task InternalMoveGroupToVoiceChannel(IRole role, IVoiceChannel fromVoiceChan, IVoiceChannel destVoiceChan, bool deleteMessage = true)
		{
			var author = ctx.Message.Author;

			if (fromVoiceChan == null)
			{
				await ctx.Channel.SendMessageAsync($"Vous devez être dans un chan vocal");
				return;
			}

			// role does not exists
			if (role == null)
			{
				await ctx.Channel.SendMessageAsync($"Le rôle {role} n'existe pas.");
				return;
			}

			// voicechan does not exists
			if (destVoiceChan == null)
			{
				await ctx.Channel.SendMessageAsync($"Le chan vocal spécifié n'existe pas.");
				return;
			}

			var usersToMove = (await fromVoiceChan.GetUsersAsync().FlattenAsync()).Where(u => u.RoleIds.Contains(role.Id));

			foreach (var el in usersToMove)
			{
				await el.ModifyAsync(a =>
				{
					a.Channel = Optional.Create(destVoiceChan);
				});
			}

			if (deleteMessage)
				await ctx.Message.DeleteAsync();

		}
	}
}